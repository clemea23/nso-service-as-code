
We have already completed [Setup](Prep.md). Go ahead with Lab Execution

# Lab Execution

> **_NOTE:_**  Please export `BRANCHNAME` with your devnet workshop station id. For example if your laptop ID is 7 use `user07`

```
export BRANCHNAME=userXX
```

## Task 1 - Review Pipeline Definition

1. Review pipeline definition https://gitlab.com/clemea23/nso-service-as-code/-/blob/main/.gitlab-ci.yml

## Task 2 - Triggering Config Changes

IP address change in the service.

1. Edit Service as a Code config file:
```
vi SaaC/sample/customer.yaml
```

2. Change IP address
```
sample:
  - name: first_service
    access-device:
      device-id: ios-0
      interface-id: 0/0/0/1
      ip-address: 10.0.0.102
```

3. Exit editor
```
<ESC>
:wq!<ENTER>
```

4. Commit changes to branch
```
git add SaaC/sample/customer.yaml
git commit -m "Task 2 - Trigger Config Change"
git push --set-upstream origin $BRANCHNAME
```

5. Open in a new tab: https://gitlab.com/clemea23/nso-service-as-code/-/pipelines

6. Review CI/CD process running with your user tag

## Task 3 - Address Validation

IP address change in the service.
Negative testcase - invalid IP address format.

1. Edit Service as a Code config file:
```
vi SaaC/sample/customer.yaml
```

2. Change IP address
```
sample:
  - name: first_service
    access-device:
    ...
      ip-address: 10.0.0.302
```

3. Exit editor
```
<ESC>
:wq!<ENTER>
```

4. Commit changes to branch
```
git add SaaC/sample/customer.yaml
git commit -m "Task 3 - Validate Input"
git push
```

5. Open in a new tab: https://gitlab.com/clemea23/nso-service-as-code/-/pipelines

6. Review CI/CD process running with your user tag - especially Test and Rollback steps

## Task 4 - Service Deletion

1. Edit Service as a Code config file:
```
vi SaaC/sample/customer.yaml
```

2. Delete `second_service` section from yaml file

3. Revert IP address to correct:
```
sample:
  - name: first_service
    access-device:
    ...
      ip-address: 10.0.0.2
```

4. Exit editor
```
<ESC>
:wq!<ENTER>
```

5. Commit changes to branch
```
git add SaaC/sample/customer.yaml
git commit -m "Task 4 - Service Deletion"
git push
```

6. Open in a new tab: https://gitlab.com/clemea23/nso-service-as-code/-/pipelines

7. Review CI/CD process running with your user tag - especially Test and Rollback steps
