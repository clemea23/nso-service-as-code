#!/bin/bash

while getopts u: flag
do
    case "${flag}" in
        u) username=${OPTARG};;
    esac
done

# Start your local docker-compose environment
# docker login registry.gitlab.com/clemea23
docker-compose -f docker-compose.yml up -d

# Verify if all containers are running. Give it some time for the NSO container to fully start
docker ps

# The gitlab-runner will be dynamic, meaning that each branch will be associated with specific runner
# We will create userX git branch and the pipeline will be executed on userX runner

# Chnage USER variable on each workshop laptop to achieve separation of environmentts
export GLUSER=$username
export TOKEN=GR1348941enXjf9A3A9h_wUzCHmC4

# now register the runner
docker exec gitlab-runner gitlab-runner register \
  --non-interactive                              \
  --name="Gitlab runner $GLUSER"                 \
  --url="https://gitlab.com/"                    \
  --registration-token="$TOKEN"                  \
  --tag-list $GLUSER                             \
  --run-untagged="false"                         \
  --executor="docker"                            \
  --docker-image="debian:buster"                 \
  --docker-network-mode="test_network"
